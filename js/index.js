/* jshint esnext: true */

// Vue.config.debug = true;

Array.prototype.clone = function() {
	return this.slice(0);
};


var vm = new Vue({

	el: "#game-area",

	data: {
		gameData: [],
		played: [],
		turn: 'X',
		moveCount: 0,
		boardSize: 3,
		winMax: 3
	},

	ready: function() {
		this.init();
	},

	methods: {
		init: function() {
			for (let i = 0; i < this.boardSize; i++) {
				this.gameData.push([]);
				this.played.push([]);
				for (let j = 0; j < this.boardSize; j++) {
					this.gameData[i].push(null);
					this.played[i].push(null);
				}
			}
		},
		resetGame: function() {
			this.$set('gameData', []);
			this.$set('played', []);
			this.$set('turn', 'X');
			this.$set('moveCount', 0);
			this.init();
		},
		checkRows: function(x, y, board) {
			for (let i = 0; i < this.winMax; i++) {
				if (board[i][y] !== board[x][y]) return false;
				if (i == this.winMax - 1) return (board[x][y] === 'X' ? 1 : -1);
			}
		},
		checkColumns: function(x, y, board) {
			for (let i = 0; i < this.winMax; i++) {
				if (board[x][i] !== board[x][y]) return false;
				if (i == this.winMax - 1) return (board[x][y] === 'X' ? 1 : -1);
			}
		},
		checkDiagonalLeftToRight: function(x, y, board) {
			if (x === y) {
				// Check from left to right diagnoal
				for (let i = 0; i < this.winMax; i++) {
					if (board[i][i] !== board[x][y]) return false;
					if (i == this.winMax - 1) return (board[x][y] === 'X' ? 1 : -1);
				}
			}
			return false;
		},
		checkDiagonalRightToLeft: function(x, y, board) {
			for (var i = 0; i < this.winMax; i++) {
				if (board[i][this.winMax - 1 - i] !== board[x][y]) return false;
				if (i == this.winMax - 1) return (board[x][y] === 'X' ? 1 : -1);
			}
		},
		isDraw: function(moveCount) {
			return moveCount === 9;
		},
		showWinningRow: function(x, y) {
			$('.play-button').attr('disabled', true);
			for (let i = 0; i < this.winMax; i++) {
				$('.play-button[data-x="' + i + '"][data-y="' + y + '"]').removeAttr('disabled');
			}
		},
		showWinningColumn: function(x, y) {
			$('.play-button').attr('disabled', true);
			for (let i = 0; i < this.winMax; i++) {
				$('.play-button[data-x="' + x + '"][data-y="' + i + '"]').removeAttr('disabled');
			}
		},
		showWinningLeftToRight: function(x, y) {
			$('.play-button').attr('disabled', true);
			for (let i = 0; i < this.winMax; i++) {
				$('.play-button[data-x="' + i + '"][data-y="' + i + '"]').removeAttr('disabled');
			}
		},
		showWinningRightToLeft: function(x, y) {
			$('.play-button').attr('disabled', true);
			for (let i = 0; i < this.winMax; i++) {
				$('.play-button[data-x="' + i + '"][data-y="' + (this.winMax - 1 - i) + '"]').removeAttr('disabled');
			}
		},
		showWinningDraw: function() {
			$('.play-button').attr('disabled', true);
		},
		playButtonMouseIn: function(x, y) {
			if (this.played[x][y] === null) {
				this.gameData[x].$set(y, this.turn);
			}
		},
		playButtonMouseOut: function(x, y) {
			if (this.played[x][y] === null) {
				this.gameData[x].$set(y, null);
			}
		},
		checkWinOrDraw: function(x, y) {
			if (this.checkRows(x, y, this.played)) this.showWinningRow(x, y);
			if (this.checkColumns(x, y, this.played)) this.showWinningColumn(x, y);
			if (this.checkDiagonalLeftToRight(x, y, this.played)) this.showWinningLeftToRight(x, y);
			if (this.checkDiagonalRightToLeft(x, y, this.played)) this.showWinningRightToLeft(x, y);
			if (this.isDraw(this.moveCount)) this.showWinningDraw();
		},
		boardScore(board) {
			// check row
			if (board[0][1] !== null && board[0][0] == board[0][1] && board[0][1] == board[0][2])
				return board[0][1] == 'X' ? 1 : -1;

			if (board[1][1] !== null && board[1][0] == board[1][1] && board[1][1] == board[1][2])
				return board[1][1] == 'X' ? 1 : -1;

			if (board[2][1] !== null && board[2][0] == board[2][1] && board[2][1] == board[2][2])
				return board[2][1] == 'X' ? 1 : -1;

			// Check column
			if (board[1][0] !== null && board[0][0] == board[1][0] && board[1][0] == board[2][0])
				return board[1][0] == 'X' ? 1 : -1;

			if (board[1][1] !== null && board[0][1] == board[1][1] && board[1][1] == board[2][1])
				return board[1][1] == 'X' ? 1 : -1;

			if (board[1][2] !== null && board[0][2] == board[1][2] && board[1][2] == board[2][2])
				return board[1][2] == 'X' ? 1 : -1;

			// Check diagonal from left to right
			if (board[1][1] !== null && board[0][0] == board[1][1] && board[1][1] == board[2][2])
				return board[1][1] == 'X' ? 1 : -1;

			// Check diagonal from right to left
			if (board[1][1] !== null && board[0][2] == board[1][1] && board[1][1] == board[2][0])
				return board[1][1] == 'X' ? 1 : -1;

			return 0;
		},
		play: function(x, y) {
			if (!this.played[x][y]) {
				this.played[x].$set(y, this.turn);
				this.turn = this.turn == 'X' ? 'O' : 'X';

				this.moveCount = (this.moveCount + 1);
				this.checkWinOrDraw(x, y);

				if (this.turn === 'O') {
					[x, y] = this.calculateNextMove();
					if (x !== null && y !== null) {
						this.played[x].$set(y, 'O');
						this.gameData[x].$set(y, 'O');
						this.turn = this.turn == 'X' ? 'O' : 'X';
						this.moveCount = (this.moveCount + 1);
						this.checkWinOrDraw(x, y);
					}

				}
			}
		},
		calculateNextMove: function() {
			// Let's Do some AI
			var output = this.playAsMinimizer(this.played.clone(), this.moveCount, -Infinity, Infinity);
			return [output.x, output.y];
		},
		playAsMaximizer(board, moveCount, alpha, beta) {
			let score = this.boardScore(board);
			let maxValue = -Infinity;
			let moveIndex = {};

			if (moveCount === 9 || score !== 0) {
				return {
					score:score * (500 - moveCount),
					x: null,
					y: null
				};
			}
			for (let i = 0; i < 3; i++) {
				for (let j = 0; j < 3; j++) {
					if (board[i][j] === null) {
						board[i][j] = 'X';
						let nextMove = this.playAsMinimizer(board, moveCount + 1, alpha, beta);

						board[i][j] = null;

						if (maxValue < nextMove.score) {
							maxValue = nextMove.score;
							moveIndex.x = i;
							moveIndex.y = j;
							alpha = (alpha > maxValue) ? alpha : maxValue;
							if (beta <= alpha) break;
						}
					}
				}
			}
			return {
				score: maxValue,
				x: moveIndex.x,
				y: moveIndex.y
			};
		},
		playAsMinimizer(board, moveCount, alpha, beta) {
			let score = this.boardScore(board);
			let minValue = Infinity;
			let moveIndex = {};

			if (moveCount === 9 || score !== 0) {
				return {
					score:score * (500 - moveCount),
					x: null,
					y: null
				};
			}
			for (let i = 0; i < 3; i++) {
				for (let j = 0; j < 3; j++) {
					if (board[i][j] === null) {
						board[i][j] = 'O';
						let nextMove = this.playAsMaximizer(board, moveCount + 1, alpha, beta);

						board[i][j] = null;

						if (minValue > nextMove.score) {
							minValue = nextMove.score;
							moveIndex.x = i;
							moveIndex.y = j;
							beta = (beta < minValue) ? beta : minValue;
							if (beta <= alpha) break;
						}
					}
				}
			}
			return {
				score: minValue,
				x: moveIndex.x,
				y: moveIndex.y
			};
		}
	}

});
